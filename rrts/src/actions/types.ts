import { DeleteTodoAction, FetchTodosAction } from '../interfaces';

export enum ActionTypes {
  fetchTodos,
  deleteTodo,
}

export type Action = FetchTodosAction | DeleteTodoAction;
