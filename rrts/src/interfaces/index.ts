import { ActionTypes } from '../actions/types';

export interface Todo {
  id: number;
  title: string;
  completed: boolean;
}

export interface StoreState {
  todos: Todo[];
}

export interface FetchTodosAction {
  type: ActionTypes.fetchTodos;
  payload: Todo[];
}

export interface DeleteTodoAction {
  type: ActionTypes.deleteTodo;
  payload: number;
}
