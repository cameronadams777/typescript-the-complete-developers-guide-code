import { combineReducers } from 'redux';
import { todosReducer } from './todos';
import { StoreState } from '../interfaces';

export const reducers = combineReducers<StoreState>({
  todos: todosReducer,
});
